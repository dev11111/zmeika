public enum SnakeState {
  ONE;
   String moving = "";
   int count = 0;
  static  int a = 0;
  int n = 7;
  boolean feedFlag = false;
   int[][] m = new int[n][n];
   int[][]temporaryStage = new int[n][n];
   String lastMoving = "";
   int snakeLength = 3;
   int gameCount = 0;
   int correntX = 1;
   int correntY = 2;

  public void setN(int n) {
    this.n = n;
  }

  public void init() {
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        if (temporaryStage[i][j] ==2 && feedFlag){
          m[i][j] = 2;
        }else {
          m[i][j] = 0;
        }
      }
    }
  }

  public void setSnake() {
    init();
    if (count == snakeLength-1) lastMoving = moving;
    m[correntY][correntX] = 1;
    if (moving == "down" && lastMoving == "right"){
      drawLineUp(correntX,correntY,count);
      drawLineLeft(correntX,correntY-count,snakeLength - count);
    }
    if (moving == "down" && lastMoving == "left"){
      drawLineUp(correntX,correntY,count);
      drawLineRight(correntX,correntY-count,snakeLength - count);
    }
    if (moving == "up" && lastMoving == "right"){
      drawLineDown(correntX,correntY,count);
      drawLineLeft(correntX,correntY+count,snakeLength - count);
    }
    if (moving == "up" && lastMoving == "left"){
      drawLineDown(correntX,correntY,count);
      drawLineRight(correntX,correntY+count,snakeLength - count);
    }
//--------------------------------------------------
    if (moving == "right" && lastMoving == "down"){
      drawLineLeft(correntX,correntY,count);
      drawLineUp(correntX-count,correntY,snakeLength - count);
    }
    if (moving == "right" && lastMoving == "up"){
      drawLineLeft(correntX,correntY,count);
      drawLineDown(correntX-count,correntY,snakeLength - count);
    }
    if (moving == "left" && lastMoving == "down"){
      drawLineRight(correntX,correntY,count);
      drawLineUp(correntX+count,correntY,snakeLength - count);
    }
    if (moving == "left" && lastMoving == "up"){
      drawLineRight(correntX,correntY,count);
      drawLineDown(correntX+count,correntY,snakeLength - count);
    }
    if (moving == lastMoving){
        init();
        if(moving == "left"){
          drawLineRight(correntX,correntY,snakeLength);
        }
      if(moving == "right"){
        drawLineLeft(correntX,correntY,snakeLength);
      }
      if(moving == "down"){
        drawLineUp(correntX,correntY,snakeLength);
      }
      if(moving == "up"){
        drawLineDown(correntX,correntY,snakeLength);
      }

    }

  }

  public void refreshCorrentState() {
    init();
//    m[correntY][correntX] = 1;
  }

  public void drawLineDown(int x, int y, int length) {
    for (int i = 0; i< length; i++){
      m[y + i][x] = 1;
    }
  }

  public void drawLineUp(int x, int y, int length) {
    for (int i = 0; i< length; i++){
      m[y - i][x] = 1;
    }
  }

  public void drawLineRight(int x, int y, int length) {
    for (int i = 0; i< length; i++){
      m[y][x + i] = 1;
    }
  }

  public void drawLineLeft(int x, int y, int length) {
    for (int i = 0; i< length; i++){
      m[y][x - i] = 1;
    }
  }

  public void doGame(int timeCount,boolean gameIsGoing) throws Exception {
    if (timeCount == this.n) {
      gameIsGoing = false;
    }
    if (temporaryStage[correntX][correntY] == 2){
      gameCount++;
      snakeLength++;
      temporaryStage[correntX][correntY] = 0;
    }

    if (this.moving == "right") {
      this.correntX++;
      this.refreshCorrentState();
    }
    if (this.moving == "left") {
      this.correntX--;
      this.refreshCorrentState();
    }
    if (this.moving == "up") {
      this.correntY--;
      this.refreshCorrentState();
    }
    if (this.moving == "down") {
      this.correntY++;
      this.refreshCorrentState();
    }
//    drawLineLeft(correntX,correntY,3);
    System.out.println("set direction");
//    System.out.println(timeCount);
  }

  public void setRandomTemporaryStage(){
    init();

    boolean b=false;
    for (int i = 0;i < n; i++){
      for (int j = 0; j < n; j++){
        temporaryStage[i][j] = 0;
        b = getRandomBoolean();
        if (b && m[i][j] != 1 && feedFlag){
          temporaryStage[i][j] = 2;
        }
      }
    }
  }
  public boolean getRandomBoolean() {
    return Math.random() < 0.2;
  }
}

